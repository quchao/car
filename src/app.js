
import regeneratorRuntime from 'regenerator-runtime';

App({
  async onLaunch (options) {
    // 声明周期 onLaunch
  },
  async onShow (options) {
    // 声明周期 onShow
  },
  async onError (msg) {
    // 生命周期 onError
  },

  // 全局状态
  globalData: {
    
  }
})
