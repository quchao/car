
// imports
import regeneratorRuntime from 'regenerator-runtime';

Page({
  data:{

  },

  async onLoad( option ){
    // 生命周期函数--监听页面加载

  },

  async onReady( option ){
    // 生命周期函数--监听页面初次渲染完成

  },

  async onShow( option ){
    // 生命周期函数--监听页面显示

  },

  async onHide(){
    //生命周期函数--监听页面隐藏
    
  },
  
  async onUnload(){
    // 生命周期函数--监听页面卸载

  },

  async onPullDownRefresh(e){
    // 页面相关事件处理函数--监听用户下拉动作

  },

  async onReachBottom(e){
    // 页面上拉触底事件的处理函数

  },

  async onShareAppMessage(e){
    // 用户点击右上角转发

  },

  async onPageScroll(e){
    // 页面滚动触发事件的处理函数

  },

  async onTabItemTap(e){
    // 当前是 tab 页时，点击 tab 时触发

  }

})
